﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Models
{
    public class RestaurantModel
    {
        public Guid ID { set; get; }
        public string Name { set; get; }
        public string Address { set; get; }
    }
}
