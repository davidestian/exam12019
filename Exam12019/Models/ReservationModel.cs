﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Models
{
    public class ReservationModel
    {
        public Guid ID { set; get; }
        
        public string Name { set; get; }
       
        public string Email { set; get; }
        
        public Guid RestaurantID { set; get; }
        
        public DateTimeOffset ReservationTime { set; get; }
    }
}
