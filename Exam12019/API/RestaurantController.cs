﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam12019.API
{
    [Route("api/restaurant")]
    [Produces("application/json")]
    public class RestaurantController : Controller
    {
        /// <summary>
        /// Add Dependency Restaurant Service To Get Data From Cache
        /// </summary>
        private readonly RestaurantService DB;
        public RestaurantController(RestaurantService restaurantSevice)
        {
            this.DB = restaurantSevice;
        }
        
        /// <summary>
        /// Get All Restaurants
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<RestaurantModel>> GetRestaurantsAsync()
        {
            var restaurants = await DB.GetRestaurantsAsync();
            return restaurants;
        }

        /// <summary>
        /// Get Restaurant Based On ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<RestaurantModel>> Get(Guid ID)
        {
            var restaurants = await DB.GetRestaurantsAsync();
            var restaurant = restaurants.Where(Q => Q.ID == ID).FirstOrDefault();
            if (restaurant == null)
            {
                return NotFound();
            }
            return restaurant;
        }

        /// <summary>
        /// Add A New Restaurant To The Cache
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<bool>> Post([FromBody]string json)
        {
            if (json == null)
            {
                return BadRequest();
            }
            
            var restaurant = JsonConvert.DeserializeObject<RestaurantModel>(json);
            await DB.CreateRestaurantAsync(restaurant);
            return true;

        }

        /// <summary>
        /// Edit A Restaurant Based On RestaurantID
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="sendJson"></param>
        /// <returns></returns>
        [HttpPost("{id}")]
        public async Task<ActionResult<bool>> Put(Guid ID, [FromBody]string sendJson)
        {
            if(sendJson == null)
            {
                return BadRequest();
            }

            var update = JsonConvert.DeserializeObject<RestaurantModel>(sendJson);

            var restaurants = await DB.GetRestaurantsAsync();
            var restaurant = restaurants.Where(Q => Q.ID == update.ID).FirstOrDefault();
            if(restaurant == null)
            {
                return NotFound();
            }
            await DB.UpdateRestaurantAsync(update);
            return true;
        }

        /// <summary>
        /// Delete A Restaurant Based On RestaurantID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> Delete(Guid ID)
        {
            if(ID == null)
            {
                return BadRequest();
            }
            var restaurants = await DB.GetRestaurantsAsync();
           
            if(restaurants.Where(Q=>Q.ID == ID).Any() == false)
            {
                return NotFound();
            }
            await DB.DeleteRestaurantAsync(ID);
            return true;
        }
    }
}
