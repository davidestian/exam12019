﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace Exam12019.API
{
    public class Reserve
    {
        [Required]
        [StringLength(255)]
        public string Name { set; get; }
        [Required]
        [StringLength(255)]
        [EmailAddress]
        public string Email { set; get; }
        [Required]
        public Guid RestaurantID { set; get; }
        [Required]
        public DateTimeOffset ReservationTime { set; get; }
    }


    [Route("api/[controller]")]
    [ApiController]
    public class ValuesApiController : ControllerBase
    {
        private readonly RestaurantService RestaurantDB;
        private readonly ReservationService ReservationDB;
        private readonly AppSettings Settings;

        /// <summary>
        /// Add Dependency To Get Restaurant And Reservation Data From Cache
        /// Add The Use Of Applicationsettings values
        /// </summary>
        /// <param name="restaurantService"></param>
        /// <param name="reservationService"></param>
        /// <param name="appSettings"></param>
        public ValuesApiController(RestaurantService restaurantService, ReservationService reservationService, AppSettings appSettings)
        {
            this.RestaurantDB = restaurantService;
            this.ReservationDB = reservationService;
            this.Settings = appSettings;
        }

        /// <summary>
        /// Get All Reservations
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<ReservationModel>> GetReservationsAsync()
        {
            var reservations = await ReservationDB.GetReservationsAsync();
            return reservations;
        }

        /// <summary>
        /// Get Reservations By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "Get")]
        public async Task<ActionResult<ReservationModel>> Get(Guid ID)
        {
            if (ID == null)
            {
                return BadRequest();
            }

            var reservations = await ReservationDB.GetReservationsAsync();
            var reservation = reservations.Where(Q => Q.ID == ID).FirstOrDefault();
            if (reservation == null)
            {
                return NotFound();
            }
            return reservation;
        }

        /// <summary>
        /// Add Reservation
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Reserve value)
        {
            //Url = http://localhost:50508/api/ValuesApi
            var IsHeaders = new StringValues();

            var checkApiKey = Request.Headers.TryGetValue("ApiKey", out IsHeaders);
            if(checkApiKey == false)
            {
                return BadRequest("Invalid API Key");
            }

            if (IsHeaders.FirstOrDefault().Equals(Settings.ReservationApiKey) == false)
            {
                return BadRequest("Invalid API Key");
            }

            if (ModelState.IsValid == false)
            {
                return BadRequest();
            }

            var restaurants = await RestaurantDB.GetRestaurantsAsync();
            if (restaurants.Where(Q => Q.ID == value.RestaurantID).Any() == false)
            {
                return BadRequest("Invalid Restaurant ID");
            }

            var reservation = new ReservationModel
            {
                ID = Guid.NewGuid(),
                Name = value.Name,
                Email = value.Email,
                RestaurantID = value.RestaurantID,
                ReservationTime = value.ReservationTime
            };
            await ReservationDB.CreateReservationAsync(reservation);
            return Ok();
        }
    }
}
