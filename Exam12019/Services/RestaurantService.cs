﻿using Exam12019.Models;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    public class RestaurantService
    {
        /// <summary>
        /// Add Dependency Cache To Make Persistent Data
        /// </summary>
        private readonly IDistributedCache _cache;
        public RestaurantService(IDistributedCache distributedCache)
        {
            this._cache = distributedCache;
        }

        /// <summary>
        /// Get All Restaurants
        /// </summary>
        /// <returns></returns>
        public async Task<List<RestaurantModel>> GetRestaurantsAsync()
        {
            var restaurants = await _cache.GetStringAsync("Restaurant");
            if (restaurants == null)
            {
                return new List<RestaurantModel>();
            }
            return JsonConvert.DeserializeObject<List<RestaurantModel>>(restaurants);
        }

        /// <summary>
        /// Update Changes On Restaurant Made From Create, Update, Delete
        /// </summary>
        /// <param name="restaurants"></param>
        /// <returns></returns>
        public async Task SaveRestaurantAsync(List<RestaurantModel> restaurants)
        {
            var json = JsonConvert.SerializeObject(restaurants);
            await _cache.SetStringAsync("Restaurant", json);
            
        }

        /// <summary>
        /// Create A Restaurant
        /// </summary>
        /// <param name="restaurant"></param>
        /// <returns></returns>
        public async Task CreateRestaurantAsync(RestaurantModel restaurant)
        {
            var restaurants = await GetRestaurantsAsync();
            restaurants.Add(restaurant);
            await SaveRestaurantAsync(restaurants);
        }
        public async Task UpdateRestaurantAsync(RestaurantModel update)
        {
            var restaurants = await GetRestaurantsAsync();
            var restaurant = restaurants.Where(Q => Q.ID == update.ID).FirstOrDefault();
            restaurant.Name = update.Name;
            restaurant.Address = update.Address;

            await SaveRestaurantAsync(restaurants);
            
        }
        public async Task DeleteRestaurantAsync(Guid ID)
        {
            var restaurants = await GetRestaurantsAsync();
            var restaurant = restaurants.Where(Q => Q.ID == ID).FirstOrDefault();
            restaurants.Remove(restaurant);
            await SaveRestaurantAsync(restaurants);
        }
    }
}
