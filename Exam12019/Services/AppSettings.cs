﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    /// <summary>
    /// Store Settings From Applicationsettings.json
    /// </summary>
    public class AppSettings
    {
        public string Accelist { set; get; }
        public string AdminPassword { set; get; }
        public string ReservationApiKey { set; get; }
    }
}
