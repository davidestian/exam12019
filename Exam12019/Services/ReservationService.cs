﻿using Exam12019.Models;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    public class ReservationService
    {
        /// <summary>
        /// Add Dependency Cache To Make Persistent Data
        /// </summary>
        private readonly IDistributedCache _cache;
        public ReservationService(IDistributedCache distributed)
        {
            this._cache = distributed;
        }

        /// <summary>
        /// Get Every Reservation From Cache
        /// </summary>
        /// <returns></returns>
        public async Task<List<ReservationModel>> GetReservationsAsync()
        {
            var reservations = await _cache.GetStringAsync("Reservations");
            if(reservations == null)
            {
                return new List<ReservationModel>();
            }
            return JsonConvert.DeserializeObject<List<ReservationModel>>(reservations);
        }

        /// <summary>
        /// Save Updated Reservation Changes From Create, Update, Delete
        /// </summary>
        /// <param name="reservations"></param>
        /// <returns></returns>
        public async Task SaveReservationAsync(List<ReservationModel> reservations)
        {
            var json = JsonConvert.SerializeObject(reservations);
            await _cache.SetStringAsync("Reservations", json);
        }

        /// <summary>
        /// Create A Reservation
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns></returns>
        public async Task CreateReservationAsync(ReservationModel reservation)
        {
            var reservations = await GetReservationsAsync();
            reservations.Add(reservation);
            await SaveReservationAsync(reservations);
        }
    }
}
