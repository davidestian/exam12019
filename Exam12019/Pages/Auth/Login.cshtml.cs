using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Exam12019.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Auth
{
    /// <summary>
    /// Login Form Model
    /// </summary>
    public class LoginViewModel
    {
        [Required]
        public string Username { set; get; }
        [Required]
        public string Password { set; get; }
    }

    public class LoginModel : PageModel
    {
        /// <summary>
        /// Add Dependency To Use Applicationsettings Values
        /// </summary>
        private readonly AppSettings Settings;
        public LoginModel(AppSettings appSettings)
        {
            this.Settings = appSettings;
        }

        /// <summary>
        /// Login Model Form Instance
        /// </summary>
        [BindProperty]
        public LoginViewModel Form { set; get; }

        /// <summary>
        /// Redirect User Back To Index If The User Already Logged In
        /// </summary>
        /// <returns></returns>
        public IActionResult OnGet()
        {
            if(User.Identity.IsAuthenticated)
            {
                return RedirectToPage("/Index");
            }
            return Page();
        }

        /// <summary>
        /// User Login Progress
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync(string returnUrl)
        {
            if (ModelState.IsValid == false)
            {
                return Page();

            }
            
            var valid = Form.Username == Settings.Accelist 
                && BCrypt.Net.BCrypt.Verify(Form.Password, Settings.AdminPassword);
            if (valid == false)
            {
                ModelState.AddModelError("Form.Password", "Invalid Username or Password");
                return Page();
            }

            var id = new ClaimsIdentity("ExamLogin");
            id.AddClaim(new Claim(ClaimTypes.Name, "Admin"));
            id.AddClaim(new Claim(ClaimTypes.NameIdentifier, Form.Username));
            id.AddClaim(new Claim(ClaimTypes.Role, "Administrator"));

            var principal = new ClaimsPrincipal(id);
            await this.HttpContext.SignInAsync("ExamLogin", principal, new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.UtcNow.AddYears(1),
                IsPersistent = true
            });
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToPage("/index");
        }
    }
}