using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace Exam12019.Pages.Reservation
{
    
    public class SearchModel : PageModel
    {
        /// <summary>
        /// Add Dependency To Make API Calls
        /// </summary>
        private readonly IHttpClientFactory HttpFac;
        public SearchModel(IHttpClientFactory httpClientFactory)
        {
            this.HttpFac = httpClientFactory;
        }

        [BindProperty]
        public ReservationModel Details { set; get; }

        [BindProperty(SupportsGet = true)]
        public Guid ID { set; get; }

        /// <summary>
        /// Get Reservation Requested By User And Show It
        /// </summary>
        /// <returns></returns>
        public async Task OnGet()
        {
            var client = HttpFac.CreateClient();
            var response = await client.GetStringAsync("http://localhost:50508/api/ValuesApi/" + ID);
            var reservation = JsonConvert.DeserializeObject<ReservationModel>(response);
            Details = new ReservationModel
            {
                ID = reservation.ID,
                Email = reservation.Email,
                Name = reservation.Name,
                RestaurantID = reservation.RestaurantID,
                ReservationTime = reservation.ReservationTime
            };
        }
    }
}