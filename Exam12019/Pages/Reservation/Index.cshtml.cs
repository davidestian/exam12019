using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace Exam12019.Pages.Reservation
{
    
    public class IndexModel : PageModel
    {
        /// <summary>
        /// Add Dependency To Make API Calls
        /// </summary>
        private readonly IHttpClientFactory HttpFac;
        public IndexModel(IHttpClientFactory httpClientFactory)
        {
            this.HttpFac = httpClientFactory;
        }
        [BindProperty]
        [Required]
        public Guid ReservationID { set; get; }
        public void OnGet()
        {

        }

        /// <summary>
        /// Search Reservation By Reservation ID From User Input
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult>  OnPost()
        {
            if(ModelState.IsValid == false)
            {
                return Page();
            }

            var client = HttpFac.CreateClient();
            var response = await client.GetStringAsync("http://localhost:50508/api/ValuesApi/" + ReservationID );
            

            return RedirectToPage("./Search", new { ID = ReservationID });
        }


        
    }
}