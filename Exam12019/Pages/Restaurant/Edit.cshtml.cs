using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace Exam12019.Pages.Restaurant
{
    /// <summary>
    /// Can Only Be Accessed By Administrator
    /// </summary>
    [Authorize(Roles = "Administrator")]
    public class EditViewModel
    {
        [Required]
        [StringLength(255)]
        public string Name { set; get; }
        [Required]
        [StringLength(2000)]
        public string Address { set; get; }
    }
    public class EditModel : PageModel
    {
        /// <summary>
        /// Add Dependency To Make API Calls
        /// </summary>
        private readonly IHttpClientFactory HttpFac;
        public EditModel(IHttpClientFactory httpClientFactory)
        {
            this.HttpFac = httpClientFactory;
        }

        [BindProperty]
        public EditViewModel Form { set; get; }

        [BindProperty(SupportsGet = true)]
        public Guid ID { set; get; }
        [TempData]
        public string Msg { set; get; }

        /// <summary>
        /// Get The Restaurant That User Want To Edit
        /// </summary>
        /// <returns></returns>
        public async Task OnGetAsync()
        {
            var client = HttpFac.CreateClient();
            var response = await client.GetStringAsync("http://localhost:50508/api/restaurant/" + ID);

            var restaurant = JsonConvert.DeserializeObject<RestaurantModel>(response);
            Form = new EditViewModel
            {
                Name = restaurant.Name,
                Address = restaurant.Address
            };

        }

        /// <summary>
        /// Update The Restaurant
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            var client = HttpFac.CreateClient();
            var response = await client.GetStringAsync("http://localhost:50508/api/restaurant/" + ID);

            var restaurant = JsonConvert.DeserializeObject<RestaurantModel>(response);

            restaurant.Name = Form.Name;
            restaurant.Address = Form.Address;

            var sendJson = JsonConvert.SerializeObject(restaurant);
            var responseSend = await client.PostAsJsonAsync("http://localhost:50508/api/restaurant/" + ID, sendJson);
            if (responseSend.IsSuccessStatusCode == false)
            {
                return BadRequest();
            }
            Msg = "Success edit restaurant";
            return RedirectToPage();
        }
    }
}