using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace Exam12019.Pages.Restaurant
{
    /// <summary>
    /// Can Only Be Accessed By Administrator
    /// </summary>
    [Authorize(Roles = "Administrator")]
    public class ShowModel : PageModel
    {
        /// <summary>
        /// Add Dependency To Make API Calls
        /// </summary>
        private readonly IHttpClientFactory HttpFac;
        public ShowModel(IHttpClientFactory httpClientFactory)
        {
            this.HttpFac = httpClientFactory;
        }
        [BindProperty]
        public List<RestaurantModel> Restaurants { set; get; }

        /// <summary>
        /// Show All Restaurants Available
        /// </summary>
        /// <returns></returns>
        public async Task OnGetAsync()
        {
            var client = HttpFac.CreateClient();
            var response = await client.GetStringAsync("http://localhost:50508/api/restaurant");
           
            Restaurants = JsonConvert.DeserializeObject<List<RestaurantModel>>(response);
        }
    }
}