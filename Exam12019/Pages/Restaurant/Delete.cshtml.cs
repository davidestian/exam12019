using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace Exam12019.Pages.Restaurant
{
    /// <summary>
    /// Can Only Be Accessed By Administrator
    /// </summary>
    [Authorize(Roles = "Administrator")]
    public class DeleteModel : PageModel
    {
        /// <summary>
        /// Add Dependency To Make API Calls
        /// </summary>
        private readonly IHttpClientFactory HttpFac;
        public DeleteModel(IHttpClientFactory httpClientFactory)
        {
            this.HttpFac = httpClientFactory;
        }

        [BindProperty(SupportsGet = true)]
        public Guid ID { set; get; }

        [BindProperty]
        public string Name { set; get; }
        [TempData]
        public string Msg { set; get; }
        /// <summary>
        /// Get Restaurant Name To Ask For Deletion
        /// </summary>
        /// <returns></returns>
        public async Task OnGetAsync()
        {
            var client = HttpFac.CreateClient();
            var response = await client.GetStringAsync("http://localhost:50508/api/restaurant/" + ID);

            var restaurant = JsonConvert.DeserializeObject<RestaurantModel>(response);
            Name = new string(restaurant.Name);
        }

        /// <summary>
        /// Delete Current Restaurant
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync()
        {
            var client = HttpFac.CreateClient();
            var response = await client.DeleteAsync("http://localhost:50508/api/restaurant/" + ID);
            if(response.IsSuccessStatusCode == false)
            {
                return BadRequest();
            }
            Msg = "Success delete restaurant";
            return RedirectToPage("./Show");
        }
    }
}