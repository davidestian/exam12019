using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace Exam12019.Pages.Restaurant
{
    /// <summary>
    /// Can Only Be Accessed By Administrator
    /// </summary>
    [Authorize(Roles = "Administrator")]
    public class CreateViewModel
    {
        [Required]
        [StringLength(255)]
        public string Name { set; get; }
        [Required]
        [StringLength(2000)]
        public string Address { set; get; }
    }
    public class CreateModel : PageModel
    {
        /// <summary>
        /// Add Dependency To Make API Calls
        /// </summary>
        private readonly IHttpClientFactory HttpFac;
        public CreateModel(IHttpClientFactory httpClientFactory)
        {
            this.HttpFac = httpClientFactory;
        }
        [TempData]
        public string Msg { set; get; }
        [BindProperty]
        public CreateViewModel Form { set; get; }
        public void OnGet()
        {

        }

        /// <summary>
        /// Create New Restaurant
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            var restaurant = new RestaurantModel
            {
                ID = Guid.NewGuid(),
                Name = Form.Name,
                Address = Form.Address
            };

            var json = JsonConvert.SerializeObject(restaurant);
            var client = HttpFac.CreateClient();
            var response = await client.PostAsJsonAsync("http://localhost:50508/api/restaurant", json);
            if(response.IsSuccessStatusCode == false)
            {
                return BadRequest();
            }
            Msg = "Success add new restaurant !";
            return RedirectToPage("./Show");
        }
    }
}